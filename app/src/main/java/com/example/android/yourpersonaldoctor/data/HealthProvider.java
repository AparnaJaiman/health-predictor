package com.example.android.yourpersonaldoctor.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.android.yourpersonaldoctor.data.HealthContract.HealthEntry;
import com.example.android.yourpersonaldoctor.data.HealthContract.ResultEntry;

public class HealthProvider extends ContentProvider {
    private static final String TAG = HealthProvider.class.getSimpleName();
    private HealthDbHelper healthDbHelper;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int HEALTH = 100;
    private static final int HEALTH_ID = 101;
    private static final int RESULT = 200;
    private static final int RESULT_ID = 201;

    static {

        uriMatcher.addURI(HealthContract.CONTENT_AUTHORITY, HealthContract.PATH_HEALTH,HEALTH);

        uriMatcher.addURI(HealthContract.CONTENT_AUTHORITY, HealthContract.PATH_HEALTH+"/#",HEALTH_ID);

        uriMatcher.addURI(HealthContract.CONTENT_AUTHORITY, HealthContract.PATH_RESULT,RESULT);

        uriMatcher.addURI(HealthContract.CONTENT_AUTHORITY, HealthContract.PATH_RESULT+"/#",RESULT_ID);
    }

    @Override
    public boolean onCreate() {
        healthDbHelper = new HealthDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = healthDbHelper.getReadableDatabase();
        Cursor cursor;
        int match = uriMatcher.match(uri);
        switch (match) {
            case HEALTH:
                cursor = db.query(HealthEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            case HEALTH_ID:
                selection = HealthEntry._ID+"=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(HealthEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case RESULT:
                cursor = db.query(
                        ResultEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case RESULT_ID:
                selection = ResultEntry._ID+"=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(
                        ResultEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(),uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case HEALTH:
                return HealthEntry.CONTENT_LIST_TYPE;
            case HEALTH_ID:
                return HealthEntry.CONTENT_ITEM_TYPE;
            case RESULT:
                return ResultEntry.CONTENT_RESULT_LIST_TYPE;
            case RESULT_ID:
                return ResultEntry.CONTENT_RESULT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final int match = uriMatcher.match(uri);
        switch (match) {
            case HEALTH:
                return insertDisease(uri, values);
            case RESULT:
                return insertResult(uri, values);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    private Uri insertResult(Uri uri, ContentValues values) {
        SQLiteDatabase db = healthDbHelper.getWritableDatabase();

        long id = db.insert(
                ResultEntry.TABLE_NAME,
                null,
                values
        );
        if (id == -1) {
            Log.e(TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri,null);

        // Once we know the ID of the new row in the table,
        // return the new URI with the ID appended to the end of it
        return ContentUris.withAppendedId(uri, id);
    }

    private Uri insertDisease(Uri uri, ContentValues values) {
        SQLiteDatabase db = healthDbHelper.getWritableDatabase();

        long id = db.insert(
                HealthEntry.TABLE_NAME,
                null,
                values
        );
        if (id == -1) {
            Log.e(TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri,null);

        // Once we know the ID of the new row in the table,
        // return the new URI with the ID appended to the end of it
        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        // Get writeable database
        SQLiteDatabase database = healthDbHelper.getWritableDatabase();
        int rowsDeleted;
        final int match = uriMatcher.match(uri);
        switch (match) {
            case HEALTH:
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(HealthEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case HEALTH_ID:
                // Delete a single row given by the ID in the URI
                selection = HealthEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(HealthEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case RESULT:
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(ResultEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case RESULT_ID:
                // Delete a single row given by the ID in the URI
                selection = ResultEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(ResultEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }
        if (rowsDeleted != 0)
        {
            getContext().getContentResolver().notifyChange(uri,null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        final int match = uriMatcher.match(uri);
        switch (match) {
            case HEALTH:
                return updateSymptom(uri, values, selection, selectionArgs);
            case HEALTH_ID:
                // For the HEALTH_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = HealthEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateSymptom(uri, values, selection, selectionArgs);
            case RESULT:
                return updateResult(uri, values, selection, selectionArgs);
            case RESULT_ID:
                // For the HEALTH_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = ResultEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateResult(uri, values, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    private int updateResult(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = healthDbHelper.getWritableDatabase();

        int rowsUpdated =  db.update(ResultEntry.TABLE_NAME,values,selection,selectionArgs);
        if (rowsUpdated!=0){
            getContext().getContentResolver().notifyChange(uri,null);
        }
        return rowsUpdated;
    }

    private int updateSymptom(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = healthDbHelper.getWritableDatabase();

        int rowsUpdated =  db.update(HealthEntry.TABLE_NAME,values,selection,selectionArgs);
        if (rowsUpdated!=0){
            getContext().getContentResolver().notifyChange(uri,null);
        }
        return rowsUpdated;
    }

}
