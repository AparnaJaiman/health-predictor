package com.example.android.yourpersonaldoctor;

import android.app.ActivityOptions;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.HealthEntry;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

//import static com.example.android.yourpersonaldoctor.R.string.no_internet_connection;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String symptomEntered;

    private AutoCompleteTextView autoCompleteTextView;
    ArrayList<String> symptoms = new ArrayList<>();
    LinearLayout emptyView;
    LinearLayout emptyView1;
    LinearLayout mainScreen;
    Button try_again_button;
    Button try_again_button1;
    private static final int RC_SIGN_IN = 123;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthSateListener;
    private boolean isConnected;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emptyView = (LinearLayout) findViewById(R.id.no_internet);
        emptyView.setVisibility(View.GONE);
        emptyView1 = (LinearLayout) findViewById(R.id.no_i);
        emptyView1.setVisibility(View.GONE);
        try_again_button = (Button) findViewById(R.id.try_again_button);
        try_again_button1 = (Button) findViewById(R.id.try_again_button2);
        mainScreen = (LinearLayout) findViewById(R.id.doctor_main_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mFirebaseAuth = FirebaseAuth.getInstance();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.snackbar_string, Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });



        final ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        isConnected = networkInfo != null &&
                networkInfo.isConnectedOrConnecting();
   //     if (networkInfo==null || !networkInfo.isConnectedOrConnecting()) {
     //       emptyView1.setVisibility(View.VISIBLE);
       //     emptyView.setVisibility(View.GONE);
         //   mainScreen.setVisibility(View.GONE);
       // }

        try_again_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm2 = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo2 = cm2.getActiveNetworkInfo();
                if (networkInfo2!=null && networkInfo2.isConnectedOrConnecting()) {
                    emptyView.setVisibility(View.GONE);
                    mainScreen.setVisibility(View.VISIBLE);
                }
            }
        });

        try_again_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm2 = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo2 = cm2.getActiveNetworkInfo();
                if (networkInfo2!=null && networkInfo2.isConnectedOrConnecting()) {

                    mFirebaseAuth.addAuthStateListener(mAuthSateListener);
                    emptyView1.setVisibility(View.GONE);
                    emptyView.setVisibility(View.GONE);
                    mainScreen.setVisibility(View.VISIBLE);
                }
            }
        });



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autocomplete_tv);

        symptoms.add("Coughing"); //q
        symptoms.add("Sneezing");
        symptoms.add("Soar throat");
        symptoms.add("Runny Nose");
        symptoms.add("Fatigue");  //q
        symptoms.add("Fever");   //q
        symptoms.add("Headache");  //q
        symptoms.add("Muscle Pain");
        symptoms.add("Chilling");
        symptoms.add("Sweating");
        symptoms.add("Diarrhea");
        symptoms.add("Body pain");
        symptoms.add("Weakness");
        symptoms.add("Abdominal pain");
        symptoms.add("Constipation");
        symptoms.add("Rash");
        symptoms.add("Vomitting");
        symptoms.add("Poor Apetite");
        symptoms.add("Chest pain");
        symptoms.add("Low blood pressure");
        symptoms.add("Dehydration");
        symptoms.add("Weight loss");
        symptoms.add("Bleeding from Gums/Nose/Mouth"); //new
        symptoms.add("Weak Pulse");
        symptoms.add("Black Stools");
        symptoms.add("Joint Pain");
        symptoms.add("Behind Eye Pain");
        symptoms.add("Excessive Hunger");
        symptoms.add("Frequent Urination");
        symptoms.add("Dry Mouth");
        symptoms.add("Blurred Vision");
        symptoms.add("Thirst");
        symptoms.add("Slow Healing Sores/Cuts/Injuries");
        symptoms.add("Pain in legs");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,symptoms);
        autoCompleteTextView.setAdapter(arrayAdapter);
        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCompleteTextView.showDropDown();
            }
        });
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ContentValues values = new ContentValues();
                values.put(HealthEntry.COLUMN_COUGHING, 0);
                values.put(HealthEntry.COLUMN_PERSISTENT_COUGHING, 0);
                values.put(HealthEntry.COLUMN_SNEEZING, 0);
                values.put(HealthEntry.COLUMN_SOAR_THROAT, 0);
                values.put(HealthEntry.COLUMN_RUNNY_NOSE, 0);
                values.put(HealthEntry.COLUMN_FATIGUE, 0);
                values.put(HealthEntry.COLUMN_FEVER, 0);
                values.put(HealthEntry.COLUMN_GENERAL_HEADACHE, 0);
                values.put(HealthEntry.COLUMN_MUSCLE_PAIN, 0);
                values.put(HealthEntry.COLUMN_EXTREME_FEVER, 0);
                values.put(HealthEntry.COLUMN_CHILLING, 0);
                values.put(HealthEntry.COLUMN_SWEATING, 0);
                values.put(HealthEntry.COLUMN_EXTREME_FATIGUE, 0);
                values.put(HealthEntry.COLUMN_DIARRHEA, 0);
                values.put(HealthEntry.COLUMN_BODY_PAIN, 0);
                values.put(HealthEntry.COLUMN_WEAKNESS, 0);
                values.put(HealthEntry.COLUMN_ABDOMINAL_PAIN, 0);
                values.put(HealthEntry.COLUMN_CONSTIPATION, 0);
                values.put(HealthEntry.COLUMN_RASH,0);
                values.put(HealthEntry.COLUMN_EXTREME_HEADACHE,0);
                values.put(HealthEntry.COLUMN_VOMITTING,0);
                values.put(HealthEntry.COLUMN_POOR_APPETITE,0);
                values.put(HealthEntry.COLUMN_DEHYDRATION,0);
                values.put(HealthEntry.COLUMN_CHEST_PAIN,0);
                values.put(HealthEntry.COLUMN_WEIGHT_LOSS,0);
                values.put(HealthEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH,0);     //new entries
                values.put(HealthEntry.COLUMN_WEAK_PULSE,0);
                values.put(HealthEntry.COLUMN_BLACK_STOOLS,0);
                values.put(HealthEntry.COLUMN_LOW_BLOOD_PRESSURE,0);
                values.put(HealthEntry.COLUMN_JOINT_PAIN,0);
                values.put(HealthEntry.COLUMN_BEHIND_EYE_PAIN,0);
                values.put(HealthEntry.COLUMN_EX_HUNGER,0);
                values.put(HealthEntry.COLUMN_FREQUENT_URINATION,0);
                values.put(HealthEntry.COLUMN_DRY_MOUTH,0);
                values.put(HealthEntry.COLUMN_BLURRED_VISION,0);
                values.put(HealthEntry.COLUMN_THIRST,0);
                values.put(HealthEntry.COLUMN_SLOW_HEALING_SORES,0);
                values.put(HealthEntry.COLUMN_PAIN_IN_LEGS,0);


                Uri uri = getContentResolver().insert(HealthEntry.CONTENT_URI,values);
                if (uri==null) {
                    Toast.makeText(MainActivity.this,
                            "Failed!!!!!!!!!!!!!!!",
                            Toast.LENGTH_LONG).show();
                    Log.d(TAG,
                            "Null values insertion failedddd with ::: uri "+uri);
                } else {
                    Log.d(TAG,
                            "Null values insertion successful with id :::: "+String.valueOf(ContentUris.parseId(uri)));
                }

                symptomEntered = arrayAdapter.getItem(position);

                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this);

                //Make keyboard go away after user chooses his symptom.
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);

                //Send intents on the basis of symptom chosen.
                if (symptomEntered.equalsIgnoreCase("headache") || symptomEntered.equalsIgnoreCase("Coughing")
                         || symptomEntered.equalsIgnoreCase("Fever") || symptomEntered.equalsIgnoreCase("Fatigue")) {
                    Intent intent = new Intent(MainActivity.this,QuestionActivity.class);
                    intent.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    intent.putExtra("symptom",symptomEntered);
                    intent.setData(ContentUris.withAppendedId(HealthEntry.CONTENT_URI,ContentUris.parseId(uri)));
                    startActivity(intent, options.toBundle());
                }
                else {
                    ContentValues values1 = new ContentValues();
                    if (symptomEntered.equalsIgnoreCase("sneezing")) {
                        values1.put(HealthEntry.COLUMN_SNEEZING,1);
                    } else if (symptomEntered.equalsIgnoreCase("soar throat")) {
                        values1.put(HealthEntry.COLUMN_SOAR_THROAT,1);
                    }  else if (symptomEntered.equalsIgnoreCase("runny nose")) {
                        values1.put(HealthEntry.COLUMN_RUNNY_NOSE,1);
                    }  else if (symptomEntered.equalsIgnoreCase("Muscle Pain")) {
                        values1.put(HealthEntry.COLUMN_MUSCLE_PAIN,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Chilling")) {
                        values1.put(HealthEntry.COLUMN_CHILLING,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Sweating")) {
                        values1.put(HealthEntry.COLUMN_SWEATING,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Diarrhea")) {
                        values1.put(HealthEntry.COLUMN_DIARRHEA,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Body pain")) {
                        values1.put(HealthEntry.COLUMN_BODY_PAIN,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Weakness")) {
                        values1.put(HealthEntry.COLUMN_WEAKNESS,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Abdominal pain")) {
                        values1.put(HealthEntry.COLUMN_ABDOMINAL_PAIN,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Constipation")) {
                        values1.put(HealthEntry.COLUMN_CONSTIPATION,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Rash")) {
                        values1.put(HealthEntry.COLUMN_RASH,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Vomitting")) {
                        values1.put(HealthEntry.COLUMN_VOMITTING,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Poor Apetite")) {
                        values1.put(HealthEntry.COLUMN_POOR_APPETITE,1);
                    } else if (symptomEntered.equalsIgnoreCase("Low blood pressure")) {
                        values1.put(HealthEntry.COLUMN_LOW_BLOOD_PRESSURE,1);
                    } else if (symptomEntered.equalsIgnoreCase("Dehydration")) {
                        values1.put(HealthEntry.COLUMN_DEHYDRATION,1);
                    } else if (symptomEntered.equalsIgnoreCase("Weight loss")) {
                        values1.put(HealthEntry.COLUMN_WEIGHT_LOSS,1);
                    }  else if (symptomEntered.equalsIgnoreCase("Bleeding from Gums/Nose/Mouth")) {    //new entries
                        values1.put(HealthEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH,1);
                    } else if (symptomEntered.equalsIgnoreCase("Weak Pulse")) {
                        values1.put(HealthEntry.COLUMN_WEAK_PULSE,1);
                    } else if (symptomEntered.equalsIgnoreCase("Black Stools")) {
                        values1.put(HealthEntry.COLUMN_BLACK_STOOLS,1);
                    } else if (symptomEntered.equalsIgnoreCase("Joint Pain")) {
                        values1.put(HealthEntry.COLUMN_JOINT_PAIN,1);
                    } else if (symptomEntered.equalsIgnoreCase("Behind Eye Pain")) {
                        values1.put(HealthEntry.COLUMN_BEHIND_EYE_PAIN,1);
                    } else if (symptomEntered.equalsIgnoreCase("Excessive Hunger")) {
                        values1.put(HealthEntry.COLUMN_EX_HUNGER,1);
                    } else if (symptomEntered.equalsIgnoreCase("Frequent Urination")) {
                        values1.put(HealthEntry.COLUMN_FREQUENT_URINATION,1);
                    } else if (symptomEntered.equalsIgnoreCase("Dry Mouth")) {
                        values1.put(HealthEntry.COLUMN_DRY_MOUTH,1);
                    } else if (symptomEntered.equalsIgnoreCase("Blurred Vision")) {
                        values1.put(HealthEntry.COLUMN_BLURRED_VISION,1);
                    } else if (symptomEntered.equalsIgnoreCase("Thirst")) {
                        values1.put(HealthEntry.COLUMN_THIRST,1);
                    } else if (symptomEntered.equalsIgnoreCase("Slow Healing Sores/Cuts/Injuries")) {
                        values1.put(HealthEntry.COLUMN_SLOW_HEALING_SORES,1);
                    } else if (symptomEntered.equalsIgnoreCase("Pain in legs")) {
                        values1.put(HealthEntry.COLUMN_PAIN_IN_LEGS, 1);
                    } else if (symptomEntered.equalsIgnoreCase("Chest Pain")) {
                        values1.put(HealthEntry.COLUMN_CHEST_PAIN,1);
                    } else {
                        //Do nothing.
                        autoCompleteTextView.setText("");
                        Toast.makeText(MainActivity.this,R.string.saved_any_other_symptom,Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int rowsUpdated = 0;
                    rowsUpdated = getContentResolver().update(uri, values1, null, null);
                    if (rowsUpdated==0) {
                        Log.d(TAG, "onItemClick: Update Failed!!!");
                    } else {
                        autoCompleteTextView.setText("");
                        Log.d(TAG, "onItemClick: Update Done!!!");
                    }
                    Intent intent = new Intent(MainActivity.this,OtherSymptomActivity.class);
                    intent.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    intent.putExtra("symptom",symptomEntered);
                    intent.setData(ContentUris.withAppendedId(HealthEntry.CONTENT_URI,ContentUris.parseId(uri)));
                    startActivity(intent, options.toBundle());
                }
            }
        });
        mAuthSateListener= new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user =firebaseAuth.getCurrentUser();
                if (user != null) {
                    // already signed in
                   // Toast.makeText(MainActivity.this,"Welcome! How are you",Toast.LENGTH_SHORT).show();
                } else {
                    // not signed in
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)

                                    .setAvailableProviders(Arrays.asList(
                                            new AuthUI.IdpConfig.GoogleBuilder().build(),
                                            new AuthUI.IdpConfig.PhoneBuilder().build(),
                                            new AuthUI.IdpConfig.EmailBuilder().build()))
                                    .setLogo(R.mipmap.logo)
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();

        mFirebaseAuth.removeAuthStateListener(mAuthSateListener);
    }

    @Override
    protected void onResume() {
        ConnectivityManager cm2 = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo2 = cm2.getActiveNetworkInfo();
        if (networkInfo2!=null && networkInfo2.isConnectedOrConnecting()) {
            mFirebaseAuth.addAuthStateListener(mAuthSateListener);
        }else{
           // drawer.setVisibility(View.VISIBLE);
            mainScreen.setVisibility(View.GONE);
            emptyView1.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);


        }
        super.onResume();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                // Sign-in succeeded, set up the UI
                Toast.makeText(this, "Signed in!", Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RESULT_CANCELED) {
                // Sign in was canceled by the user, finish the activity
                Toast.makeText(this, "Sign in canceled", Toast.LENGTH_SHORT).show();
                finish();
            }


        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*//noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_reports) {
            // Handle the my reports action
            Intent intent = new Intent(this,MyReportsActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_help) {
            Intent i=new Intent(MainActivity.this,HelpActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_about) {
            // Handle the about action
            Intent i=new Intent(MainActivity.this,AboutActivity.class);
            startActivity(i);

        } else if (id == R.id.Sign_out) {
            ConnectivityManager cm2 = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo2 = cm2.getActiveNetworkInfo();
            if (networkInfo2!=null && networkInfo2.isConnectedOrConnecting()) {
            AuthUI.getInstance()
                    .signOut(this);
               // mFirebaseAuth.addAuthStateListener(mAuthSateListener);
            return true;}
            else{
                Toast.makeText(MainActivity.this,"No network Access! Cannot Sign out",Toast.LENGTH_SHORT).show();
            }
            //AuthUI.getInstance()
              //      .signOut(this);
           // return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
