package com.example.android.yourpersonaldoctor;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.ResultEntry;

public class EditReportActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int LOADER_ID = 2;
    private Uri intentUri;
    ProgressBar progressBar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_report);

        Intent intent = getIntent();
        intentUri = intent.getData();
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        getLoaderManager().initLoader(LOADER_ID,null,this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_report,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_confirmation_mssg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteReport();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog!=null)
                {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteReport() {
        if (intentUri!=null) {
            int deletedRows = getContentResolver().delete(intentUri, null, null);
            if (deletedRows == 0) {
                Toast.makeText(this, R.string.delete_failed, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.delete_successful, Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,intentUri,null,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        progressBar.setVisibility(View.GONE);
        TextView disease_predicted = findViewById(R.id.scored_label);
        TextView description = findViewById(R.id.description);
        TextView causes = findViewById(R.id.causes);
        TextView food_intake = findViewById(R.id.food_intake);
        TextView further_analysis = findViewById(R.id.result_tv);
        TextView symptoms_present = findViewById(R.id.symptoms_present);
        StringBuilder stringBuilder = new StringBuilder();
        if (data.moveToFirst()) {
            String scored_label_string = String.valueOf(data.getString(data.getColumnIndex(ResultEntry.COLUMN_SCORED_LABEL)));
            disease_predicted.setText(scored_label_string);
            ResultActivity resultActivity = new ResultActivity();
            resultActivity.setupResult(scored_label_string,description,causes,food_intake);
            further_analysis.setText(String.valueOf(data.getString(data.getColumnIndex(ResultEntry.COLUMN_FURTHER_ANALYSIS))));
            for (int i=1; i<data.getColumnCount();i++) {
                if (String.valueOf(data.getString(i)).equalsIgnoreCase("1")) {
                    stringBuilder.append("->  "+String.valueOf(data.getColumnName(i))+"\n\n");
                }
            }
            symptoms_present.setText(stringBuilder.toString());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
