package com.example.android.yourpersonaldoctor;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.HealthEntry;

import java.util.ArrayList;

public class OtherSymptomActivity extends AppCompatActivity {
    private static final String TAG = OtherSymptomActivity.class.getSimpleName();
    private AutoCompleteTextView autoCompleteTextView;
    ArrayList<String> symptoms = new ArrayList<>();
    private String symptomEntered;
    Constants.AnimType type;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_symptom);

        Intent intent = getIntent();
        final Uri intentUri = intent.getData();
        Log.d(TAG, "onCreate:::::::"+String.valueOf(ContentUris.parseId(intentUri)));
        type = (Constants.AnimType) intent.getSerializableExtra(Constants.KEY_TYPE);

        setupAnimation();

        autoCompleteTextView = findViewById(R.id.autocomplete_other_tv);

        symptoms.add("Coughing"); //q
        symptoms.add("Sneezing");
        symptoms.add("Soar throat");
        symptoms.add("Runny Nose");
        symptoms.add("Fatigue");  //q
        symptoms.add("Fever");   //q
        symptoms.add("Headache");  //q
        symptoms.add("Muscle Pain");
        symptoms.add("Chilling");
        symptoms.add("Sweating");
        symptoms.add("Diarrhea");
        symptoms.add("Body pain");
        symptoms.add("Weakness");
        symptoms.add("Abdominal pain");
        symptoms.add("Constipation");
        symptoms.add("Rash");
        symptoms.add("Vomitting");
        symptoms.add("Poor Apetite");
        symptoms.add("Chest pain");
        symptoms.add("Low blood pressure");
        symptoms.add("Dehydration");
        symptoms.add("Weight loss");
        symptoms.add("Bleeding from Gums/Nose/Mouth"); //new
        symptoms.add("Weak Pulse");
        symptoms.add("Black Stools");
        symptoms.add("Joint Pain");
        symptoms.add("Behind Eye Pain");
        symptoms.add("Excessive Hunger");
        symptoms.add("Frequent Urination");
        symptoms.add("Dry Mouth");
        symptoms.add("Blurred Vision");
        symptoms.add("Thirst");
        symptoms.add("Slow Healing Sores/Cuts/Injuries");
        symptoms.add("Pain in legs");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,symptoms);
        autoCompleteTextView.setAdapter(arrayAdapter);
        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCompleteTextView.showDropDown();
            }
        });
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                symptomEntered = arrayAdapter.getItem(position);
                //hide keyboard
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                //send intents
                if (symptomEntered.equalsIgnoreCase("headache") || symptomEntered.equalsIgnoreCase("Coughing")
                        || symptomEntered.equalsIgnoreCase("Fever") || symptomEntered.equalsIgnoreCase("Fatigue")) {
                    Intent intent = new Intent(OtherSymptomActivity.this,QuestionActivity.class);
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(OtherSymptomActivity.this);
                    intent.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    intent.putExtra("symptom",symptomEntered);
                    intent.setData(intentUri);
                    startActivity(intent, options.toBundle());
                }
                else {
                    ContentValues values = new ContentValues();
                    if (symptomEntered.equalsIgnoreCase("sneezing")) {
                        values.put(HealthEntry.COLUMN_SNEEZING,1);
                    } else if (symptomEntered.equalsIgnoreCase("soar throat")) {
                        values.put(HealthEntry.COLUMN_SOAR_THROAT,1);
                    }  else if (symptomEntered.equalsIgnoreCase("runny nose")) {
                        values.put(HealthEntry.COLUMN_RUNNY_NOSE,1);
                    }  else if (symptomEntered.equalsIgnoreCase("Muscle Pain")) {
                        values.put(HealthEntry.COLUMN_MUSCLE_PAIN,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Chilling")) {
                        values.put(HealthEntry.COLUMN_CHILLING,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Sweating")) {
                        values.put(HealthEntry.COLUMN_SWEATING,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Diarrhea")) {
                        values.put(HealthEntry.COLUMN_DIARRHEA,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Body pain")) {
                        values.put(HealthEntry.COLUMN_BODY_PAIN,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Weakness")) {
                        values.put(HealthEntry.COLUMN_WEAKNESS,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Abdominal pain")) {
                        values.put(HealthEntry.COLUMN_ABDOMINAL_PAIN,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Constipation")) {
                        values.put(HealthEntry.COLUMN_CONSTIPATION,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Rash")) {
                        values.put(HealthEntry.COLUMN_RASH,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Vomitting")) {
                        values.put(HealthEntry.COLUMN_VOMITTING,1);
                    }   else if (symptomEntered.equalsIgnoreCase("Poor Apetite")) {
                        values.put(HealthEntry.COLUMN_POOR_APPETITE,1);
                    } else if (symptomEntered.equalsIgnoreCase("Low blood pressure")) {
                        values.put(HealthEntry.COLUMN_LOW_BLOOD_PRESSURE,1);
                    } else if (symptomEntered.equalsIgnoreCase("Dehydration")) {
                        values.put(HealthEntry.COLUMN_DEHYDRATION,1);
                    } else if (symptomEntered.equalsIgnoreCase("Weight loss")) {
                        values.put(HealthEntry.COLUMN_WEIGHT_LOSS,1);
                    }  else if (symptomEntered.equalsIgnoreCase("Bleeding from Gums/Nose/Mouth")) {    //new entries
                        values.put(HealthEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH,1);
                    } else if (symptomEntered.equalsIgnoreCase("Weak Pulse")) {
                        values.put(HealthEntry.COLUMN_WEAK_PULSE,1);
                    } else if (symptomEntered.equalsIgnoreCase("Black Stools")) {
                        values.put(HealthEntry.COLUMN_BLACK_STOOLS,1);
                    } else if (symptomEntered.equalsIgnoreCase("Joint Pain")) {
                        values.put(HealthEntry.COLUMN_JOINT_PAIN,1);
                    } else if (symptomEntered.equalsIgnoreCase("Behind Eye Pain")) {
                        values.put(HealthEntry.COLUMN_BEHIND_EYE_PAIN,1);
                    } else if (symptomEntered.equalsIgnoreCase("Excessive Hunger")) {
                        values.put(HealthEntry.COLUMN_EX_HUNGER,1);
                    } else if (symptomEntered.equalsIgnoreCase("Frequent Urination")) {
                        values.put(HealthEntry.COLUMN_FREQUENT_URINATION,1);
                    } else if (symptomEntered.equalsIgnoreCase("Dry Mouth")) {
                        values.put(HealthEntry.COLUMN_DRY_MOUTH,1);
                    } else if (symptomEntered.equalsIgnoreCase("Blurred Vision")) {
                        values.put(HealthEntry.COLUMN_BLURRED_VISION,1);
                    } else if (symptomEntered.equalsIgnoreCase("Thirst")) {
                        values.put(HealthEntry.COLUMN_THIRST,1);
                    } else if (symptomEntered.equalsIgnoreCase("Slow Healing Sores/Cuts/Injuries")) {
                        values.put(HealthEntry.COLUMN_SLOW_HEALING_SORES,1);
                    } else if (symptomEntered.equalsIgnoreCase("Pain in legs")) {
                        values.put(HealthEntry.COLUMN_PAIN_IN_LEGS, 1);
                    } else if (symptomEntered.equalsIgnoreCase("Chest Pain")) {
                        values.put(HealthEntry.COLUMN_CHEST_PAIN,1);
                    } else {
                        //Do nothing.
                        autoCompleteTextView.setText("");
                        Toast.makeText(OtherSymptomActivity.this,R.string.saved_any_other_symptom,Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int rowsUpdated = 0;
                    rowsUpdated = getContentResolver().update(intentUri, values, null, null);
                    if (rowsUpdated==0) {
                        Log.d(TAG, "onItemClick: Update Failed!!!!!");
                    } else {
                        autoCompleteTextView.setText("");
                        Toast.makeText(OtherSymptomActivity.this,R.string.saved_any_other_symptom,Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onItemClick: Update Done!!!!!!!");

                    }

                }
            }
        });

        //set a listener on the NO TV.
        TextView noTV = (TextView) findViewById(R.id.no_tv);
        noTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtherSymptomActivity.this,ResultActivity.class);
                i.setData(intentUri);
                startActivity(i);
            }
        });

    }

    private void setupAnimation() {
        switch (type) {
            case ExplodeJava:
                Explode enterTransition = new Explode();
                enterTransition.setDuration(1000);
                getWindow().setEnterTransition(enterTransition);
                break;
            case ExplodeXML:
                Transition explodeAnimation = TransitionInflater.from(this).inflateTransition(R.transition.explode);
                explodeAnimation.setDuration(1000);
                getWindow().setEnterTransition(explodeAnimation);
                break;
            case SlideJava:
                Slide enterAnimation = new Slide();
                enterAnimation.setSlideEdge(Gravity.RIGHT);
                enterAnimation.setDuration(1000);
                getWindow().setEnterTransition(enterAnimation);
            case FadeJava:
                Fade fadeTransition = new Fade();
                fadeTransition.setDuration(1000);
                getWindow().setEnterTransition(fadeTransition);
                break;
        }
    }

    private void showBackConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.discard_test_alert);
        builder.setPositiveButton(R.string.discard, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(OtherSymptomActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog!=null)
                {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        showBackConfirmationDialog();
    }
}
