package com.example.android.yourpersonaldoctor;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class HelpActivity extends AppCompatActivity{
    private static final int RC_SIGN_IN = 123;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthSateListener;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.help_activity);
        TextView view= findViewById(R.id.head);
        view.setText("How Can We Help ?");
        TextView view1= findViewById(R.id.tv_rep);
        view1.setText("Wanna see your previous reports?");
        TextView view2=  findViewById(R.id.tv_so);
        view2.setText("Wanna Sign out?");
        TextView view3= findViewById(R.id.tv_contact);
        view3.setText("Learn about developers");
        Button B1=(Button) findViewById(R.id.but_rep);
        Button B2=(Button) findViewById(R.id.but_so);
        Button B3=(Button) findViewById(R.id.new_button);
        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HelpActivity.this,MyReportsActivity.class);
                startActivity(intent);
            }
        });
        B2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm2 = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo2 = cm2.getActiveNetworkInfo();
                if (networkInfo2!=null && networkInfo2.isConnectedOrConnecting()) {
                    AuthUI.getInstance()
                            .signOut(HelpActivity.this);
                    mFirebaseAuth.addAuthStateListener(mAuthSateListener);
                    //Intent intent=new Intent(HelpActivity.this,MainActivity.class);
                    //startActivity(intent);
                    // mFirebaseAuth.addAuthStateListener(mAuthSateListener);
                    }
                else{
                    Toast.makeText(HelpActivity.this,"No network Access! Cannot Sign out",Toast.LENGTH_SHORT).show();
                }
            }
        });
        B3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HelpActivity.this,AboutActivity.class);
                startActivity(intent);
            }
        });

        mAuthSateListener= new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user =firebaseAuth.getCurrentUser();
                if (user != null) {
                    // already signed in
                    // Toast.makeText(MainActivity.this,"Welcome! How are you",Toast.LENGTH_SHORT).show();
                } else {
                    // not signed in
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)

                                    .setAvailableProviders(Arrays.asList(
                                            new AuthUI.IdpConfig.GoogleBuilder().build(),
                                            new AuthUI.IdpConfig.PhoneBuilder().build(),
                                            new AuthUI.IdpConfig.EmailBuilder().build()))
                                    .setLogo(R.mipmap.logo)
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };

    }
}
