package com.example.android.yourpersonaldoctor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        TextView t=(TextView) findViewById(R.id.head);
        t.setText("About Developers:");
        TextView T=(TextView) findViewById(R.id.about);
        T.setText("The app helps user to identify any disease in accordance to the symptoms encountered. It is developed by an enthusiastic team of android app developers and data engineers.");
        TextView t1=(TextView) findViewById(R.id.dev_1);
        t1.setText("Aparna Jaiman");
        TextView t2=(TextView) findViewById(R.id.dev_2);
        t2.setText("Gaurav Gupta");
        TextView t3=(TextView) findViewById(R.id.dev_3);
        t3.setText("Mayank Arora");
        TextView t4=(TextView) findViewById(R.id.dev_4);
        t4.setText("Priyanka Patel");
    }
}

