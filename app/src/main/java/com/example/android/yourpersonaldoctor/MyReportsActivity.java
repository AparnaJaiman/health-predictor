package com.example.android.yourpersonaldoctor;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.ResultEntry;

public class MyReportsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = MyReportsActivity.class.getSimpleName();
    private static final int LOADER_ID = 1;
    private MyReportsCursorAdapter mAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reports);

        ListView listView = (ListView) findViewById(R.id.listview_my_reports);
        View emptyView = findViewById(R.id.empty_view);
        listView.setEmptyView(emptyView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MyReportsActivity.this,EditReportActivity.class);
                intent.setData(ContentUris.withAppendedId(ResultEntry.CONTENT_RESULT_URI,id));
                startActivity(intent);
            }
        });
        mAdapter = new MyReportsCursorAdapter(this,null);
        listView.setAdapter(mAdapter);
        getLoaderManager().initLoader(LOADER_ID,null,this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,ResultEntry.CONTENT_RESULT_URI,null,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
