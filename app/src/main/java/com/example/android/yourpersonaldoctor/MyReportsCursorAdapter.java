package com.example.android.yourpersonaldoctor;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.ResultEntry;

public class MyReportsCursorAdapter extends CursorAdapter {

    public MyReportsCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.my_reports_list_item,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView date_tv = view.findViewById(R.id.date_list_item);
        TextView scored_label_tv = view.findViewById(R.id.scored_label_list_item);

        String date = String.valueOf(cursor.getString(cursor.getColumnIndex(ResultEntry.COLUMN_DATE)));
        String scored_label = String.valueOf(cursor.getString(cursor.getColumnIndex(ResultEntry.COLUMN_SCORED_LABEL)));

        date_tv.setText(date);
        scored_label_tv.setText(scored_label);
    }
}
