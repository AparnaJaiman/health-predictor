package com.example.android.yourpersonaldoctor;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.HealthEntry;

public class QuestionActivity extends AppCompatActivity {
    private static final String TAG = QuestionActivity.class.getSimpleName();
    TextView questionTV;
    TextView option1TV;
    TextView option2TV;
    Constants.AnimType type;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        questionTV = (TextView) findViewById(R.id.question);
        option1TV = (TextView) findViewById(R.id.option1);
        option2TV = (TextView) findViewById(R.id.option2);

        final Intent intent = getIntent();
        type = (Constants.AnimType) intent.getSerializableExtra(Constants.KEY_TYPE);
        setupAnimation();
        String symptom = intent.getStringExtra("symptom");
        final Uri intentUri = intent.getData();
        Log.d(TAG, "onCreate: Symptom is :: "+symptom);
        Log.d(TAG, "onCreate: id :::::::::::::::"+String.valueOf(ContentUris.parseId(intentUri)));
        final ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(QuestionActivity.this);
        if (symptom.equalsIgnoreCase("headache")) {
            questionTV.setText(R.string.headache_question);
            option1TV.setText(R.string.headache_option1);
            option2TV.setText(R.string.headache_option2);
            option1TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_GENERAL_HEADACHE,1);
                    values.put(HealthEntry.COLUMN_EXTREME_HEADACHE,0);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });

            option2TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_GENERAL_HEADACHE,0);
                    values.put(HealthEntry.COLUMN_EXTREME_HEADACHE,1);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!!!!");
                    } else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });
        }

        else if (symptom.equalsIgnoreCase("Coughing")) {
            questionTV.setText(R.string.coughing_question);
            option1TV.setText(R.string.coughing_option1);
            option2TV.setText(R.string.coughing_option2);
            option1TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_COUGHING,1);
                    values.put(HealthEntry.COLUMN_PERSISTENT_COUGHING,0);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });

            option2TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_COUGHING,0);
                    values.put(HealthEntry.COLUMN_PERSISTENT_COUGHING,1);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });
        }

        else if (symptom.equalsIgnoreCase("Fever")) {
            questionTV.setText(R.string.fever_question);
            option1TV.setText(R.string.fever_option1);
            option2TV.setText(R.string.fever_option2);
            option1TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_FEVER,1);
                    values.put(HealthEntry.COLUMN_EXTREME_FEVER,0);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });
            option2TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_FEVER,0);
                    values.put(HealthEntry.COLUMN_EXTREME_FEVER,1);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });
        }

        else if (symptom.equalsIgnoreCase("Fatigue")) {
            questionTV.setText(R.string.fatigue_question);
            option1TV.setText(R.string.fatigue_option1);
            option2TV.setText(R.string.fatigue_option2);
            option1TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_FATIGUE,1);
                    values.put(HealthEntry.COLUMN_EXTREME_FATIGUE,0);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });
            option2TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    values.put(HealthEntry.COLUMN_FATIGUE,0);
                    values.put(HealthEntry.COLUMN_EXTREME_FATIGUE,1);
                    int rowsUpdated = getContentResolver().update(intentUri,values,null,null);
                    if (rowsUpdated==0)
                    {
                        Log.d(TAG, "onClick: Update Failed!!!");
                    }else
                    {
                        Log.d(TAG, "onClick: Update Successful!!!!!!!");
                    }
                    Intent i = new Intent(QuestionActivity.this,OtherSymptomActivity.class);
                    i.putExtra(Constants.KEY_TYPE,Constants.AnimType.ExplodeJava);
                    i.setData(intentUri);
                    startActivity(i,options.toBundle());
                }
            });
        }
    }

    private void setupAnimation() {
        switch (type) {
            case ExplodeJava:
                Explode enterTransition = new Explode();
                enterTransition.setDuration(1000);
                getWindow().setEnterTransition(enterTransition);
                break;
            case ExplodeXML:
                Transition explodeAnimation = TransitionInflater.from(this).inflateTransition(R.transition.explode);
                explodeAnimation.setDuration(1000);
                getWindow().setEnterTransition(explodeAnimation);
                break;
            case FadeJava:
                Fade fadeTransition = new Fade();
                fadeTransition.setDuration(1000);
                getWindow().setEnterTransition(fadeTransition);
                break;
        }
    }

    private void showBackConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.discard_test_alert);
        builder.setPositiveButton(R.string.discard, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(QuestionActivity.this,MainActivity.class);
                startActivity(intent);
                finishAfterTransition();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog!=null)
                {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        showBackConfirmationDialog();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finishAfterTransition();
        return true;
    }
}
