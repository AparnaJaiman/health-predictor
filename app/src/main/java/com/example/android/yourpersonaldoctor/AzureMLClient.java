package com.example.android.yourpersonaldoctor;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.*;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class AzureMLClient {
    private static final String TAG = AzureMLClient.class.getSimpleName();
    private static String endPointURL; //Azure ML Endpoint
    private static String key; //API KEY
    private static String[] resultStringArray = new String[13];
    public AzureMLClient(String mEndPointURL,String mKey)
    {
        endPointURL= mEndPointURL;
        key= mKey;
    }
    /*
     Takes an Azure ML Request Body then Returns the Response String Which Contains Scored Lables etc
    */
    public static String[] requestResponse( String requestBody ) throws Exception
    {
        URL u = createUrl(endPointURL);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ key);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        OutputStreamWriter wr=new OutputStreamWriter(conn.getOutputStream());

        wr.write(requestBody);
        //wr.close();
        wr.flush();
        BufferedReader in = null;
        StringBuilder stringBuilder = new StringBuilder();
        int status =conn.getResponseCode();
        if(status<400){
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String decodedString;
            decodedString = in.readLine();
            while (decodedString != null) {
                stringBuilder.append(decodedString);
                decodedString = in.readLine();
            }
        }else{
            in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            //read response
            String decodedString;
            decodedString = in.readLine();
            while (decodedString != null) {
                stringBuilder.append(decodedString);
                decodedString = in.readLine();
            }
        }

        Log.d(TAG, "requestResponse: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+stringBuilder.toString());

        String[] final_result = extractFeatureFromJson(stringBuilder.toString());
        return final_result;
    }

    public static String[] extractFeatureFromJson(String string) {

        if (TextUtils.isEmpty(string))
        {
            return null;
        }

        try {

            JSONObject baseJsonResponse = new JSONObject(string);

            JSONObject resultsObject = baseJsonResponse.optJSONObject("Results");

            JSONArray output1Array = resultsObject.optJSONArray("output1");

             if (output1Array.length()>0) {
                 JSONObject currentOutput = output1Array.optJSONObject(0);
                 resultStringArray[0] = currentOutput.getString("Scored Probabilities for Class \"Chest Cold\""); //String chest_cold
                 resultStringArray[1] = currentOutput.getString("Scored Probabilities for Class \"Cholera\""); //String cholera
                 resultStringArray[2] = currentOutput.getString("Scored Probabilities for Class \"Common Cold\"");  //String common_cold
                 resultStringArray[3] = currentOutput.getString("Scored Probabilities for Class \"Dengue Fever\""); //String dengue fever
                 resultStringArray[4] = currentOutput.getString("Scored Probabilities for Class \"Dengue hemorrhagic fever\""); //String dengue hemorrhagic fever
                 resultStringArray[5] = currentOutput.getString("Scored Probabilities for Class \"Dengue shock syndrome\""); //String Dengue shock syndrome
                 resultStringArray[6] = currentOutput.getString("Scored Probabilities for Class \"Diabetes Type 1\""); //String Diabetes Type 1
                 resultStringArray[7] = currentOutput.getString("Scored Probabilities for Class \"Diabetes Type 2\""); //String Diabetes Type 2
                 resultStringArray[8] = currentOutput.getString("Scored Probabilities for Class \"Malaria\""); //String malaria
                 resultStringArray[9] = currentOutput.getString("Scored Probabilities for Class \"Tuberclosis\""); // String tuberculosis
                 resultStringArray[10] = currentOutput.getString("Scored Probabilities for Class \"Typhoid\""); //String typhoid
                 resultStringArray[11] = currentOutput.getString("Scored Probabilities for Class \"Viral Fever\""); //String Viral Fever
                 resultStringArray[12] = currentOutput.getString("Scored Labels"); //String scored_labels
             }

        } catch (JSONException e) {
            Log.e(TAG, "Problem parsing the JSON results", e);
        }

        return resultStringArray;
    }

    public static URL createUrl(String string)
    {
        URL url = null;
        try {
            url = new URL(string);
        } catch (MalformedURLException e) {
            Log.d(TAG, "createUrl: Error in creating URL for string : "+string);
            e.printStackTrace();
            return null;
        }
        return url;
    }

}
