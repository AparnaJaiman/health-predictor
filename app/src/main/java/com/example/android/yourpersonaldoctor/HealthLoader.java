package com.example.android.yourpersonaldoctor;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

public class HealthLoader extends AsyncTaskLoader<String[]> {
    private static String mRequestBody;
    private String[] response = null;
    private static final String TAG = HealthLoader.class.getSimpleName();
    private String mMLEndPoint;
    private String mMLApiKey;
    AzureMLClient myMLClient;

    public HealthLoader(Context context, String requestBody, String mlEndPoint, String mlApiKey) {
        super(context);
        mRequestBody = requestBody;
        mMLEndPoint = mlEndPoint;
        mMLApiKey = mlApiKey;
    }

    @Override
    public String[] loadInBackground() {
        if (mRequestBody==null || mMLEndPoint==null || mMLApiKey==null)
        {
            return null;
        }
        myMLClient = new AzureMLClient(mMLEndPoint,mMLApiKey);
        try {
            response = myMLClient.requestResponse(mRequestBody);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
