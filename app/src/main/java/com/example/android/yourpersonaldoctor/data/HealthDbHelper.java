package com.example.android.yourpersonaldoctor.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.android.yourpersonaldoctor.MainActivity;
import com.example.android.yourpersonaldoctor.data.HealthContract.HealthEntry;
import com.example.android.yourpersonaldoctor.data.HealthContract.ResultEntry;

public class HealthDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "doctor.db";
    public HealthDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_HEALTH_TABLE =
                "CREATE TABLE "
                        +HealthEntry.TABLE_NAME
                        +" ("+HealthEntry._ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                        +HealthEntry.COLUMN_COUGHING +" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_PERSISTENT_COUGHING+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_FEVER+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_EXTREME_FEVER+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_FATIGUE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_EXTREME_FATIGUE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_GENERAL_HEADACHE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_EXTREME_HEADACHE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_SNEEZING+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_SOAR_THROAT+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_RUNNY_NOSE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_MUSCLE_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_CHILLING+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_SWEATING+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_DIARRHEA+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_BODY_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_WEAKNESS+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_ABDOMINAL_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_CONSTIPATION+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_RASH+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_VOMITTING+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_POOR_APPETITE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_DEHYDRATION+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_CHEST_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_WEAK_PULSE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_BLACK_STOOLS+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_LOW_BLOOD_PRESSURE+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_JOINT_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_BEHIND_EYE_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_EX_HUNGER+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_FREQUENT_URINATION+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_DRY_MOUTH+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_BLURRED_VISION+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_THIRST+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_SLOW_HEALING_SORES+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_PAIN_IN_LEGS+" INTEGER NOT NULL DEFAULT 0, "
                        +HealthEntry.COLUMN_WEIGHT_LOSS+" INTEGER NOT NULL DEFAULT 0);";

        db.execSQL(SQL_CREATE_HEALTH_TABLE);


        String SQL_CREATE_RESULT_TABLE =
                "CREATE TABLE "
                        +ResultEntry.TABLE_NAME
                        +" ("+ResultEntry._ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                        +ResultEntry.COLUMN_DATE +" TEXT, "
                        +ResultEntry.COLUMN_COUGHING +" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_PERSISTENT_COUGHING+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_FEVER+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_EXTREME_FEVER+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_FATIGUE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_EXTREME_FATIGUE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_GENERAL_HEADACHE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_EXTREME_HEADACHE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_SNEEZING+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_SOAR_THROAT+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_RUNNY_NOSE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_MUSCLE_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_CHILLING+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_SWEATING+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_DIARRHEA+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_BODY_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_WEAKNESS+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_ABDOMINAL_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_CONSTIPATION+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_RASH+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_VOMITTING+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_POOR_APPETITE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_DEHYDRATION+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_CHEST_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_WEAK_PULSE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_BLACK_STOOLS+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_LOW_BLOOD_PRESSURE+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_JOINT_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_BEHIND_EYE_PAIN+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_EX_HUNGER+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_FREQUENT_URINATION+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_DRY_MOUTH+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_BLURRED_VISION+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_THIRST+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_SLOW_HEALING_SORES+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_PAIN_IN_LEGS+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_WEIGHT_LOSS+" INTEGER NOT NULL DEFAULT 0, "
                        +ResultEntry.COLUMN_FURTHER_ANALYSIS +" TEXT, "
                        +ResultEntry.COLUMN_SCORED_LABEL+" TEXT);";

        db.execSQL(SQL_CREATE_RESULT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Nothing for now.
    }
}
