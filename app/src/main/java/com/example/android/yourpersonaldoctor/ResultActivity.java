package com.example.android.yourpersonaldoctor;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.yourpersonaldoctor.AzureMLClient;
import com.example.android.yourpersonaldoctor.data.HealthContract;
import com.example.android.yourpersonaldoctor.data.HealthContract.ResultEntry;
import com.example.android.yourpersonaldoctor.data.HealthContract.HealthEntry;
import com.example.android.yourpersonaldoctor.data.HealthDbHelper;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.microsoft.windowsazure.mobileservices.http.MobileServiceHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ResultActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String[]> {
    private static final String TAG = ResultActivity.class.getSimpleName();
    private static final int LOADER_ID = 1;
    String mlEndPoint;
    String mlAPIKey;
    private String date;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    String further_analysis=null;
    String scored_label_string;
    JSONObject postData = new JSONObject();
    private String[] diseases = new String[]{"Chest Cold","Cholera","Common Cold","Dengue Fever","Dengue hemorrhagic fever","Dengue shock syndrome","Diabetes Type 1","Diabetes Type 2","Malaria","Tuberculosis","Typhoid","Viral Fever"};
    private String coughing, persistent_coughing, sneezing,soar_throat,runny_nose,fatigue,fever,general_headache,muscle_pain,
        extreme_fever,chilling,sweating,extreme_fatigue,diarrhea,body_pain,weakness,abdominal_pain,constipation,rash,
        extreme_headache,vomitting,poor_appetite,dehydration,chest_pain,weight_loss,bleeding_gums,weak_pulse,black_stools,
        low_blood_pressure,joint_pain,behind_eye_pain,excessive_hunger,frequent_urination,dry_mouth,blurred_vision,
        thirst,slow_healing_sores,pain_in_legs;
    PieChart pieChart;
    public float chest_cold=0,cholera,common_cold=0,dengue_fever=0,dengue_hemorrhagic_fever=0,dengue_shock_syndrome=0,
            diabetes_type_1=0,diabetes_type_2=0,malaria=0,tuberculosis=0,typhoid=0,viral_fever=0;
    private float[] yData;
    private String[] xData = {"Chest Cold","Cholera","Common Cold","Dengue Fever","Dengue Hemorrhagic Fever","Dengue Shock Syndrome",
            "Diabetes Type 1", "Diabetes Type_2","Malaria","Tuberculosis","Typhoid","Viral Fever"};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        Uri intentUri = intent.getData();
        Log.d(TAG, "onCreate: id ::::::::::::"+String.valueOf(ContentUris.parseId(intentUri)));


        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        linearLayout = (LinearLayout) findViewById(R.id.report_linear_layout);
        linearLayout.setVisibility(View.GONE);
        pieChart = (PieChart) findViewById(R.id.pie_chart);

        pieChart.getDescription().setText(getString(R.string.pie_chart_description));
        pieChart.getDescription().setTextColor(Color.DKGRAY);
        pieChart.getDescription().setTextSize(15f);
        pieChart.setRotationEnabled(true);
        pieChart.setUsePercentValues(true);
        pieChart.setHoleRadius(25f);
        pieChart.setHoleColor(0xFFffffff);
        /*pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(255); // fully opaque*/
        //pieChart.setTransparentCircleAlpha(0);
        pieChart.setTransparentCircleRadius(38f);
        pieChart.setCenterText(getString(R.string.piechart_center_text));
        pieChart.setCenterTextColor(Color.DKGRAY);
        pieChart.setCenterTextSize(15);

        Calendar calendar = Calendar.getInstance();
        int mYear = calendar.get(Calendar.YEAR);
        int monthTemp = calendar.get(Calendar.MONTH);
        int mMonth = monthTemp+1;
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        date = mDay+"/"+mMonth+"/"+mYear;
        Log.d(TAG, "onCreate: Date is ::::::::"+date);

        Cursor cursor = getContentResolver().query(intentUri,null,null,null,null);
            if (cursor.moveToFirst()) {
                coughing = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_COUGHING)));
                persistent_coughing = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_PERSISTENT_COUGHING)));
                sneezing = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_SNEEZING)));
                soar_throat = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_SOAR_THROAT)));
                runny_nose = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_RUNNY_NOSE)));
                fatigue = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_FATIGUE)));
                fever = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_FEVER)));
                general_headache = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_GENERAL_HEADACHE)));
                muscle_pain = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_MUSCLE_PAIN)));
                extreme_fever = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_EXTREME_FEVER)));
                chilling = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_CHILLING)));
                sweating = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_SWEATING)));
                extreme_fatigue = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_EXTREME_FATIGUE)));
                diarrhea = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_DIARRHEA)));
                body_pain = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_BODY_PAIN)));
                weakness = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_WEAKNESS)));
                abdominal_pain = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_ABDOMINAL_PAIN)));
                constipation = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_CONSTIPATION)));
                rash = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_RASH)));
                extreme_headache = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_EXTREME_HEADACHE)));
                vomitting = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_VOMITTING)));
                poor_appetite = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_POOR_APPETITE)));
                dehydration = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_DEHYDRATION)));
                chest_pain = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_CHEST_PAIN)));
                weight_loss = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_WEIGHT_LOSS)));
                bleeding_gums = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH)));  //new
                weak_pulse = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_WEAK_PULSE)));
                black_stools = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_BLACK_STOOLS)));
                low_blood_pressure = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_LOW_BLOOD_PRESSURE)));
                joint_pain = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_JOINT_PAIN)));
                behind_eye_pain = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_BEHIND_EYE_PAIN)));
                excessive_hunger = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_EX_HUNGER)));
                frequent_urination = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_FREQUENT_URINATION)));
                dry_mouth = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_DRY_MOUTH)));
                blurred_vision = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_BLURRED_VISION)));
                thirst = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_THIRST)));
                slow_healing_sores = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_SLOW_HEALING_SORES)));
                pain_in_legs = String.valueOf(cursor.getString(cursor.getColumnIndex(HealthEntry.COLUMN_PAIN_IN_LEGS)));
            }
        mlEndPoint = "https://ussouthcentral.services.azureml.net/workspaces/ed009d4eea5c4799a069a9baf2122882/services/ba7032be30bf42c3b47677ddc07ff6d5/execute?api-version=2.0&format=swagger";
        mlAPIKey= "fR3/Qg6eXR1l3NyoN68ZcGcEQxWf/U8L+8CVIXLCixzvQQoFKjpcc9tgVchGoiee+poNYmMqOmVGL5ZkX+gnkw==";
        /*requestBody = "\"Inputs\":{\"input1\":{\"ColumnNames\":[\"Coughing\",\"Persistent Coughing\",\"Sneezing\",\"Soar Throat\",\"Runny/Blocked Nose\",\"Fatigue\",\"Fever\",\"General Headache\",\"Muscle Pain\",\"Extreme Fever\",\"Chilling\",\"Sweating\",\"Extreme Fatigue\",\"Diarreha\",\"Body Pain\",\"Weakness\",\"Abdominal Pain\",\"Constipation\",\"Rash\",\"Extreme Headacahe\",\"Vomitting\",\"Poor Appetite\",\"Dehydration\",\"Chest Pain\",\"Weight Loss\",\"Disease\"]," +
                "\"Values\":[["+coughing+","+persistent_coughing+","+sneezing+","+soar_throat+","+runny_nose+","+fatigue+","+fever+","+general_headache+","+muscle_pain+","+extreme_fever+","+chilling+","+sweating+","+extreme_fatigue+","+diarrhea+","+body_pain+","+weakness+","+abdominal_pain+","+constipation+","+rash+","+extreme_headache+","+vomitting+","+poor_appetite+","+dehydration+","+chest_pain+","+weight_loss+","+""+"]]}},\"GlobalParameters\":{}}";
*/
        try {
            JSONObject value = new JSONObject();
            value.put("Persistent Coughing", persistent_coughing);
            value.put("Sneezing", sneezing);
            value.put("Fever", fever);
            value.put("Chilling", chilling);
            value.put("Sweating", sweating);
            value.put("Diarreha", diarrhea);
            value.put("Constipation", constipation);
            value.put("General Headache", general_headache);
            value.put("Soar Throat", soar_throat);
            value.put("Runny/Blocked Nose", runny_nose);
            value.put("Dehydration", dehydration);
            value.put("Chest Pain", chest_pain);
            value.put("Weight Loss", weight_loss);
            value.put("Coughing", coughing);
            value.put("Poor Appetite", poor_appetite);
            value.put("Bleeding from Gums/Nose/Mouth", bleeding_gums);
            value.put("Weak Pulse", weak_pulse);
            value.put("Black Stools", black_stools);
            value.put("Abdominal Pain", abdominal_pain);
            value.put("Low Blood Pressure", low_blood_pressure);
            value.put("Vomitting", vomitting);
            value.put("Extreme Headacahe", extreme_headache);
            value.put("Extreme Fever", extreme_fever);
            value.put("Rash", rash);
            value.put("Weakness", weakness);
            value.put("Muscle Pain", muscle_pain);
            value.put("Joint Pain", joint_pain);
            value.put("Behind Eye Pain", behind_eye_pain);
            value.put("Body Pain", body_pain);
            value.put("Extreme Fatigue", extreme_fatigue);
            value.put("Fatigue", fatigue);
            value.put("Excessive Hunger", excessive_hunger);
            value.put("Frequent Urination", frequent_urination);
            value.put("Dry Mouth", dry_mouth);
            value.put("Blurred Vision", blurred_vision);
            value.put("Thirster", thirst);
            value.put("Slow Healing Sores/Cuts/Injuries", slow_healing_sores);
            value.put("Pain in legs", pain_in_legs);
            value.put("Disease", "");

            JSONArray input1 = new JSONArray();
            input1.put(value);
            JSONObject Inputs = new JSONObject();
            Inputs.put("input1", input1);
            postData.put("Inputs", Inputs);
        } catch (JSONException e) {
            e.printStackTrace();
        }
            getLoaderManager().initLoader(LOADER_ID,null,this);
    }

    private void addDataSet()
    {
        Log.d(TAG, "addDataSet: Started function!!");
        ArrayList<PieEntry> yEntries = new ArrayList<>();
        ArrayList<String> xEntries = new ArrayList<>();
        for (int i=0;i<yData.length;i++)
        {
            if (yData[i]>1) {
                yEntries.add(new PieEntry(yData[i], i));
            }
        }
        for (int i=0;i<xData.length;i++)
        {
            xEntries.add(xData[i]);
        }
        PieDataSet pieDataSet = new PieDataSet(yEntries,getString(R.string.y_axis_label));
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.MAGENTA);
        colors.add(0xFF795548);
        colors.add(0xFFffcdd2);
        colors.add(0xFFffc107);
        colors.add(0xFFc6ff00);
        colors.add(0xFF90a4ae);
        colors.add(0xFF9c27b0);
        colors.add(Color.CYAN);
        colors.add(Color.RED);
        //colors.add(R.color.pieColor7);
        colors.add(Color.GREEN);
        colors.add(Color.BLUE);
        //colors.add(R.color.pieColor2);
        colors.add(Color.YELLOW);
       /* colors.add(R.color.pieColor3);
        colors.add(R.color.pieColor1);
        colors.add(R.color.pieColor6);*/
        pieDataSet.setColors(colors);
        pieDataSet.setSliceSpace(0f);

        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);
        legend.setTextSize(13f);

        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieData.setValueTextSize(0f);
        pieData.setValueFormatter(new PercentFormatter());
        pieChart.setDrawSliceText(false);
        pieChart.invalidate();
    }

    private void checkIfOnlyOneSymptomEntered() {
        int count = 0;
        String[] symptomsArray = {coughing,persistent_coughing,sneezing,soar_throat,runny_nose,fatigue,fever,general_headache,
                muscle_pain,extreme_fever,chilling,sweating,extreme_fatigue,diarrhea,body_pain,weakness,abdominal_pain,constipation,
                rash,extreme_headache,vomitting,poor_appetite,dehydration,chest_pain,weight_loss,bleeding_gums,weak_pulse,
                black_stools,low_blood_pressure,joint_pain,behind_eye_pain,excessive_hunger,frequent_urination,dry_mouth,blurred_vision,
                thirst,slow_healing_sores,pain_in_legs};
        for (int i=0;i<symptomsArray.length;i++) {
            if (symptomsArray[i].equalsIgnoreCase("1")) {
                count++;
            }
        }
        if (count==1) {
            Toast.makeText(this,R.string.not_enough_symptoms,Toast.LENGTH_LONG).show();
        } else {
            //do nothing
        }
    }

    private void showBackConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_report_alert);
        builder.setPositiveButton(R.string.continue_without_saving, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(ResultActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton(R.string.stay_here, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog!=null)
                {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        showBackConfirmationDialog();
    }

    @Override
    public Loader<String[]> onCreateLoader(int id, Bundle args) {
        return new HealthLoader(this,postData.toString(),mlEndPoint,mlAPIKey);
    }

    @Override
    public void onLoadFinished(Loader<String[]> loader, String[] data) {

        if (data!=null && data.length>0) {
            checkIfOnlyOneSymptomEntered();
            linearLayout.setVisibility(View.VISIBLE);
            StringBuilder stringBuilder = new StringBuilder();
            TextView scored_label = findViewById(R.id.scored_label);
            scored_label.setText(data[12]);
            TextView description = findViewById(R.id.description);
            TextView causes = findViewById(R.id.causes);
            TextView food_intake = findViewById(R.id.food_intake);

            setupResult(data[12],description,causes,food_intake);

            chest_cold = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[0])*100)); //chest_cold
            cholera = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[1])*100)); //String cholera
            common_cold = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[2])*100));//String common_cold
            dengue_fever = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[3])*100)); //String dengue fever
            dengue_hemorrhagic_fever = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[4])*100)); //String dengue hemorrhagic fever
            dengue_shock_syndrome = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[5])*100));; //String Dengue shock syndrome
            diabetes_type_1 = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[6])*100));; //String Diabetes Type 1
            diabetes_type_2 = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[7])*100)); //String Diabetes Type 2
            malaria = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[8])*100)); //String malaria
            tuberculosis = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[9])*100));; // String tuberculosis
            typhoid = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[10])*100)); //String typhoid
            viral_fever = Float.parseFloat(new DecimalFormat("##.##").format(Double.parseDouble(data[11])*100)); //String Viral Fever

            Log.d(TAG, "onLoadFinished: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! chest coldddd!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+chest_cold+"!!!!!!!!!!!!!!!!!!!!!"+cholera+"!!!!!!!!!!!"+common_cold);

            scored_label_string = data[12];

            for (int i=0;i<data.length-1;i++) {
                if (Double.parseDouble(data[i])>0.10) {
                    String result = String.valueOf(new DecimalFormat("##.##").format(Double.parseDouble(data[i])*100))+"%";
                    String resultStatement = "->   "+result+" of people with such symptoms have "+diseases[i]+".\n\n";
                    stringBuilder.append(resultStatement);
                }
            }
            TextView result_tv = findViewById(R.id.result_tv);
            further_analysis = stringBuilder.toString();
            result_tv.setText(further_analysis);
        }
        progressBar.setVisibility(View.INVISIBLE);

        yData = new float[]{chest_cold,cholera,common_cold,dengue_fever,
                dengue_hemorrhagic_fever,dengue_shock_syndrome,diabetes_type_1,
                diabetes_type_2,malaria,tuberculosis,typhoid,
                viral_fever};
        addDataSet();

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.d(TAG, "onValueSelected: Value Selected from Chart");
                Log.d(TAG, "onValueSelected: "+e.toString());
                Log.d(TAG, "onValueSelected: "+h.toString());
                int first = e.toString().indexOf("y");
                int pos1 = e.toString().indexOf("y", first + 1);
                String percent = e.toString().substring(pos1 + 3);
                for (int i=0; i<yData.length;i++)
                {
                    if (yData[i]==Float.parseFloat(percent))
                    {
                        pos1=i;
                        break;
                    }
                }
                String disease = xData[pos1];
                Toast.makeText(getApplicationContext(),"Disease : "+disease,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private void insertIntoMyReports(String scored_label, String further_analysis) {
        ContentValues values = new ContentValues();
        values.put(ResultEntry.COLUMN_DATE,date);
        values.put(ResultEntry.COLUMN_FURTHER_ANALYSIS,further_analysis);
        values.put(ResultEntry.COLUMN_SCORED_LABEL,scored_label);
        values.put(ResultEntry.COLUMN_COUGHING, coughing);
        values.put(ResultEntry.COLUMN_PERSISTENT_COUGHING, persistent_coughing);
        values.put(ResultEntry.COLUMN_SNEEZING, sneezing);
        values.put(ResultEntry.COLUMN_SOAR_THROAT, soar_throat);
        values.put(ResultEntry.COLUMN_RUNNY_NOSE, runny_nose);
        values.put(ResultEntry.COLUMN_FATIGUE, fatigue);
        values.put(ResultEntry.COLUMN_FEVER, fever);
        values.put(ResultEntry.COLUMN_GENERAL_HEADACHE, general_headache);
        values.put(ResultEntry.COLUMN_MUSCLE_PAIN, muscle_pain);
        values.put(ResultEntry.COLUMN_EXTREME_FEVER, extreme_fever);
        values.put(ResultEntry.COLUMN_CHILLING, chilling);
        values.put(ResultEntry.COLUMN_SWEATING, sweating);
        values.put(ResultEntry.COLUMN_EXTREME_FATIGUE, extreme_fatigue);
        values.put(ResultEntry.COLUMN_DIARRHEA, diarrhea);
        values.put(ResultEntry.COLUMN_BODY_PAIN, body_pain);
        values.put(ResultEntry.COLUMN_WEAKNESS, weakness);
        values.put(ResultEntry.COLUMN_ABDOMINAL_PAIN, abdominal_pain);
        values.put(ResultEntry.COLUMN_CONSTIPATION, constipation);
        values.put(ResultEntry.COLUMN_RASH,rash);
        values.put(ResultEntry.COLUMN_EXTREME_HEADACHE,extreme_headache);
        values.put(ResultEntry.COLUMN_VOMITTING,vomitting);
        values.put(ResultEntry.COLUMN_POOR_APPETITE,poor_appetite);
        values.put(ResultEntry.COLUMN_DEHYDRATION,dehydration);
        values.put(ResultEntry.COLUMN_CHEST_PAIN,chest_pain);
        values.put(ResultEntry.COLUMN_WEIGHT_LOSS,weight_loss);
        values.put(ResultEntry.COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH,bleeding_gums);     //new entries
        values.put(ResultEntry.COLUMN_WEAK_PULSE,weak_pulse);
        values.put(ResultEntry.COLUMN_BLACK_STOOLS,black_stools);
        values.put(ResultEntry.COLUMN_LOW_BLOOD_PRESSURE,low_blood_pressure);
        values.put(ResultEntry.COLUMN_JOINT_PAIN,joint_pain);
        values.put(ResultEntry.COLUMN_BEHIND_EYE_PAIN,behind_eye_pain);
        values.put(ResultEntry.COLUMN_EX_HUNGER,excessive_hunger);
        values.put(ResultEntry.COLUMN_FREQUENT_URINATION,frequent_urination);
        values.put(ResultEntry.COLUMN_DRY_MOUTH,dry_mouth);
        values.put(ResultEntry.COLUMN_BLURRED_VISION,blurred_vision);
        values.put(ResultEntry.COLUMN_THIRST,thirst);
        values.put(ResultEntry.COLUMN_SLOW_HEALING_SORES,slow_healing_sores);
        values.put(ResultEntry.COLUMN_PAIN_IN_LEGS,pain_in_legs);

        Uri uri = getContentResolver().insert(ResultEntry.CONTENT_RESULT_URI,values);
        if (uri==null) {
            Toast.makeText(ResultActivity.this,
                    R.string.report_saving_failed,
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this,R.string.report_saved,Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
    }

    public void setupResult(String scored_label, TextView description, TextView causes, TextView food_intake) {
            if (scored_label.equalsIgnoreCase("Chest cold")) {
                description.setText(R.string.chest_cold_description);
                causes.setText(R.string.chest_cold_causes);
                food_intake.setText(R.string.chest_cold_food_intake);
            } else if (scored_label.equalsIgnoreCase("Common Cold")) {
                description.setText(R.string.common_cold_description);
                causes.setText(R.string.common_cold_causes);
                food_intake.setText(R.string.common_cold_food_intake);
            } else if (scored_label.equalsIgnoreCase("Malaria")) {
                description.setText(R.string.malaria_description);
                causes.setText(R.string.malaria_causes);
                food_intake.setText(R.string.malaria_food_intake);
            } else if (scored_label.equalsIgnoreCase("Typhoid")) {
                description.setText(R.string.typhoid_description);
                causes.setText(R.string.typhoid_causes);
                food_intake.setText(R.string.typhoid_food_intake);
            } else if (scored_label.equalsIgnoreCase("Cholera")) {
                description.setText(R.string.cholera_description);
                causes.setText(R.string.cholera_causes);
                food_intake.setText(R.string.cholera_food_intake);
            } else if (scored_label.equalsIgnoreCase("Tuberclosis")) {
                description.setText(R.string.tuberculosis_description);
                causes.setText(R.string.tuberculosis_causes);
                food_intake.setText(R.string.tuberculosis_food_intake);
            } else if (scored_label.equalsIgnoreCase("Dengue Fever") || scored_label.equalsIgnoreCase("Dengue hemorrhagic fever") || scored_label.equalsIgnoreCase("Dengue shock syndrome")) {
                description.setText(R.string.dengue_description);
                causes.setText(R.string.dengue_causes);
                food_intake.setText(R.string.dengue_food_intake);
            } else if (scored_label.equalsIgnoreCase("Diabetes Type 1")) {
                description.setText(R.string.diabetes_1_description);
                causes.setText(R.string.diabetes_1_causes);
                food_intake.setText(R.string.diabetes_1_food_intake);
            } else if (scored_label.equalsIgnoreCase("Diabetes Type 2")) {
                description.setText(R.string.diabetes_2_description);
                causes.setText(R.string.diabetes_2_causes);
                food_intake.setText(R.string.diabetes_2_food_intake);
            } else if (scored_label.equalsIgnoreCase("Viral Fever")) {
                description.setText(R.string.viral_fever_description);
                causes.setText(R.string.viral_fever_causes);
                food_intake.setText(R.string.viral_fever_food_intake);
            }
        }

    @Override
    public void onLoaderReset(Loader<String[]> loader) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_result,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_report:
                showSaveConfirmationDialog();
                return true;

             default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void showSaveConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.save_confirmation_mssg);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                insertIntoMyReports(scored_label_string,further_analysis);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog!=null)
                {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}