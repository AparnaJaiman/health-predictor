package com.example.android.yourpersonaldoctor.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class HealthContract {
    public static final String CONTENT_AUTHORITY = "com.example.android.yourpersonaldoctor";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://"+CONTENT_AUTHORITY);
    public static final String PATH_HEALTH = "health";
    public static final String PATH_RESULT = "result";

    private HealthContract() {}

    public static final class HealthEntry implements BaseColumns{

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI,PATH_HEALTH);
        /**
         * The MIME type of the #CONTENT_URI for a list of symptoms.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_HEALTH;

        /**
         * The MIME type of the CONTENT_URI for a single symptom.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_HEALTH;

        public static final String TABLE_NAME="health";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_COUGHING="coughing";
        public static final String COLUMN_PERSISTENT_COUGHING="persistent_coughing";
        public static final String COLUMN_FEVER="fever";
        public static final String COLUMN_EXTREME_FEVER="extreme_fever";
        public static final String COLUMN_FATIGUE="fatigue";
        public static final String COLUMN_EXTREME_FATIGUE="extreme_fatigue";
        public static final String COLUMN_GENERAL_HEADACHE="general_headache";
        public static final String COLUMN_EXTREME_HEADACHE="extreme_headache";
        public static final String COLUMN_SNEEZING="sneezing";
        public static final String COLUMN_SOAR_THROAT="soar_throat";
        public static final String COLUMN_RUNNY_NOSE="runny_nose";
        public static final String COLUMN_MUSCLE_PAIN="muscle_pain";
        public static final String COLUMN_CHILLING="chilling";
        public static final String COLUMN_SWEATING="sweating";
        public static final String COLUMN_DIARRHEA="diarrhea";
        public static final String COLUMN_BODY_PAIN="body_pain";
        public static final String COLUMN_WEAKNESS="weakness";
        public static final String COLUMN_ABDOMINAL_PAIN="abdominal_pain";
        public static final String COLUMN_CONSTIPATION="constipation";
        public static final String COLUMN_RASH="rash";
        public static final String COLUMN_VOMITTING="vomitting";
        public static final String COLUMN_POOR_APPETITE="poor_appetite";
        public static final String COLUMN_DEHYDRATION="dehydration";
        public static final String COLUMN_CHEST_PAIN="chest_pain";
        public static final String COLUMN_WEIGHT_LOSS="weight_loss";
        public static final String COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH="bleeding_gums_nose_mouth";  //new
        public static final String COLUMN_WEAK_PULSE="weak_pulse";
        public static final String COLUMN_BLACK_STOOLS="black_stools";
        public static final String COLUMN_LOW_BLOOD_PRESSURE="low_blood_pressure";
        public static final String COLUMN_JOINT_PAIN="joint_pain";
        public static final String COLUMN_BEHIND_EYE_PAIN="behind_eye_pain";
        public static final String COLUMN_EX_HUNGER="excessive_hunger";
        public static final String COLUMN_FREQUENT_URINATION="frequent_urination";
        public static final String COLUMN_DRY_MOUTH="dry_mouth";
        public static final String COLUMN_BLURRED_VISION="blurred_vision";
        public static final String COLUMN_THIRST="thirst";
        public static final String COLUMN_SLOW_HEALING_SORES="slow_healing_sores_cuts_injuries";
        public static final String COLUMN_PAIN_IN_LEGS="pain_in_legs";
    }

    public static final class ResultEntry implements BaseColumns{

        public static final Uri CONTENT_RESULT_URI = Uri.withAppendedPath(BASE_CONTENT_URI,PATH_RESULT);
        /**
         * The MIME type of the #CONTENT_RESULT_URI for a list of symptoms.
         */
        public static final String CONTENT_RESULT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_RESULT;

        /**
         * The MIME type of the CONTENT_RESULT_URI for a single symptom.
         */
        public static final String CONTENT_RESULT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_RESULT;

        public static final String TABLE_NAME="result";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_COUGHING="coughing";
        public static final String COLUMN_PERSISTENT_COUGHING="persistent_coughing";
        public static final String COLUMN_FEVER="fever";
        public static final String COLUMN_EXTREME_FEVER="extreme_fever";
        public static final String COLUMN_FATIGUE="fatigue";
        public static final String COLUMN_EXTREME_FATIGUE="extreme_fatigue";
        public static final String COLUMN_GENERAL_HEADACHE="general_headache";
        public static final String COLUMN_EXTREME_HEADACHE="extreme_headache";
        public static final String COLUMN_SNEEZING="sneezing";
        public static final String COLUMN_SOAR_THROAT="soar_throat";
        public static final String COLUMN_RUNNY_NOSE="runny_nose";
        public static final String COLUMN_MUSCLE_PAIN="muscle_pain";
        public static final String COLUMN_CHILLING="chilling";
        public static final String COLUMN_SWEATING="sweating";
        public static final String COLUMN_DIARRHEA="diarrhea";
        public static final String COLUMN_BODY_PAIN="body_pain";
        public static final String COLUMN_WEAKNESS="weakness";
        public static final String COLUMN_ABDOMINAL_PAIN="abdominal_pain";
        public static final String COLUMN_CONSTIPATION="constipation";
        public static final String COLUMN_RASH="rash";
        public static final String COLUMN_VOMITTING="vomitting";
        public static final String COLUMN_POOR_APPETITE="poor_appetite";
        public static final String COLUMN_DEHYDRATION="dehydration";
        public static final String COLUMN_CHEST_PAIN="chest_pain";
        public static final String COLUMN_WEIGHT_LOSS="weight_loss";
        public static final String COLUMN_BLEEDING_FROM_GUMS_NOSE_MOUTH="bleeding_gums_nose_mouth";  //new
        public static final String COLUMN_WEAK_PULSE="weak_pulse";
        public static final String COLUMN_BLACK_STOOLS="black_stools";
        public static final String COLUMN_LOW_BLOOD_PRESSURE="low_blood_pressure";
        public static final String COLUMN_JOINT_PAIN="joint_pain";
        public static final String COLUMN_BEHIND_EYE_PAIN="behind_eye_pain";
        public static final String COLUMN_EX_HUNGER="excessive_hunger";
        public static final String COLUMN_FREQUENT_URINATION="frequent_urination";
        public static final String COLUMN_DRY_MOUTH="dry_mouth";
        public static final String COLUMN_BLURRED_VISION="blurred_vision";
        public static final String COLUMN_THIRST="thirst";
        public static final String COLUMN_SLOW_HEALING_SORES="slow_healing_sores_cuts_injuries";
        public static final String COLUMN_PAIN_IN_LEGS="pain_in_legs";
        public static final String COLUMN_SCORED_LABEL="scored_label";
        public static final String COLUMN_FURTHER_ANALYSIS = "further_analysis";
    }
}
